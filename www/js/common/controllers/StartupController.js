/**
 * Created by kHaLiL on 30/04/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.controllers = parkly.modules.controllers || angular.module('parkly.controllers', []);

// Création du controleur pour la vue Login
parkly.modules.controllers
		.controller('StartupController', ['$scope', '$state', 'jwtRefreshService', '$rootScope', '$log', '$ionicHistory', '$cordovaSplashscreen',
			'$auth', '$localStorage', 'AuthenticationService', '$cordovaDialogs', 'defaultLocation', '$q', '$cordovaNetwork', '$interval', '$timeout',
			'Utils',
			function ($scope, $state, jwtRefreshService, $rootScope, $log, $ionicHistory, $cordovaSplashscreen, $auth, $localStorage,
					  AuthenticationService, $cordovaDialogs, defaultLocation, $q, $cordovaNetwork, $interval, $timeout, Utils) {

				$log.debug('StartupController !');

				$scope.$on('$ionicView.afterEnter', function(){
					if (window.cordova) {
						setTimeout(function () {
							$cordovaSplashscreen.hide();
						}, 100);
					}

					AuthenticationService.getUser().then(function (user) {
						$ionicHistory.nextViewOptions({
							disableBack: true
						});

						AuthenticationService.goToHomeOrUserCar(user);
					}, function (error) {
						$cordovaDialogs.alert('Une erreur est survenue lors de la récupération de vos informations utilisateur. Veuillez vous reconnecter.', 'Erreur')
								.then(function(){
									$state.go('welcome');
								});
					});

					Utils.updateColors();
				});

				$rootScope.logout = function () {
					$log.debug('Logout : Déclenchement');

					if($rootScope.getSpotsInterval) {$interval.cancel($rootScope.getSpotsInterval);}

					AuthenticationService.logout().finally(function(){
						$log.debug('Logout : Logout serveur OK');
						$auth.logout().then(function () {
							$log.debug('Logout : Auth Logout OK');

							// Facebook logout
							facebookConnectPlugin.logout(function(){
								$log.debug('Logout : Logout Facebook OK');
							}, function(fail){
								$log.debug('Logout : Déconnexion FB impossible');
							});

							// Google logout
							window.plugins.googleplus.logout(
									function (msg) {
										$log.debug('Google logout : ', msg);
									},
									function(fail){
										$log.debug('Google logout : ', fail);
									}
							);

							// Clear app data
							$localStorage.remove('_st');
							$localStorage.remove('_st_spot');
							$localStorage.remove('_st_ttl');
							$localStorage.remove('user');
							$localStorage.remove('_satellizer_token');
							$localStorage.remove('push_regId');
							$localStorage.remove('demandesEnvoyees');
							$localStorage.remove('demandesRecues');
							$rootScope.currentUser = null;
							$timeout(function(){
								$state.go('welcome');
							}, 100);
						}, function(error){
							$log.warn('Logout : Auth Logout KO');
						});
					}, function(error){
						$log.warn('Logout : Logout serveur KO');
					});
				};

				$rootScope.goTo = function(state){
					$state.go(state);
				};
			}
		]);