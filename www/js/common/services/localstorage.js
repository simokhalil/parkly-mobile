/**
 * Created by kHaLiL on 19/03/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.services = parkly.modules.services || angular.module('parkly.services', []);

// Création du controleur pour la vue Login
parkly.modules.services
		.factory('$localStorage', ['$window', function ($window) {
			return {
				set: function (key, value) {
					$window.localStorage[key] = value;
				},
				get: function (key, defaultValue) {
					defaultValue = defaultValue || null;
					if(typeof $window.localStorage[key] === 'undefined' || $window.localStorage[key] === 'undefined') {
						$window.localStorage.removeItem(key);
						return defaultValue;
					}
					return $window.localStorage[key] || defaultValue;
				},
				setObject: function (key, value) {
					$window.localStorage[key] = angular.toJson(value);
				},
				getObject: function (key, defaultValue) {
					if(typeof $window.localStorage[key] === 'undefined' || $window.localStorage[key] === 'undefined') {
						$window.localStorage.removeItem(key);
						return defaultValue;
					}
					return angular.fromJson($window.localStorage[key] || defaultValue);
				},
				getInt: function(key, defaultValue){
					if(typeof $window.localStorage[key] === 'undefined' || $window.localStorage[key] === 'undefined') {
						$window.localStorage.removeItem(key);
						return defaultValue;
					}
					return parseInt($window.localStorage[key] || defaultValue);
				},
				remove: function(key){
					$window.localStorage.removeItem(key);
				}
			};
		}]);
