/**
 * Created by kHaLiL on 20/03/2016.
 */



// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.services = parkly.modules.services || angular.module('parkly.services', []);

// Création du controleur pour la vue Login
parkly.modules.services
		.service('NotificationService', ['$rootScope', '$localStorage', 'APIBaseURL', '$ionicModal', '$http', '$q', '$cordovaDevice', '$log',
			'$cordovaMedia', '$cordovaDialogs', '$timeout',
			function ($rootScope, $localStorage, APIBaseURL, $ionicModal, $http, $q, $cordovaDevice, $log,
														  $cordovaMedia, $cordovaDialogs, $timeout) {
				var _self = this;

				_self.regId = null;

				_self.pushService = null;

				$rootScope.notification = {};
				$rootScope.notification.userRating = {
					iconOn: 'ion-ios-star',
					iconOff: 'ion-ios-star-outline',
					iconOnColor: 'rgb(0, 0, 0)',
					iconOffColor: 'rgb(0, 0, 0)',
					rating: 0,
					minRating: 0,
					readOnly:true
				};

				_self.register = function () {
					$log.debug('NotificationService.register : GCM registration...');

					var deferred = $q.defer();

					_self.pushService = PushNotification.init({
						android: {
							senderID: '96181691286'
						},
						ios: {
							alert: 'true',
							badge: 'true',
							sound: 'true'
						}
					});

					_self.pushService.on('registration', function (result) {
						$log.info('NotificationService.register : GCM Device registration success : ', result);

						_self.regId = result.registrationId;
						$localStorage.set('push_regId', result.registrationId);

						if(ionic.Platform.isAndroid){
							_self.storeDeviceToken('android');
						} else if (ionic.Platform.isIOS()) {
							_self.storeDeviceToken('ios');
						}
					}, function (err) {
						$log.warn('NotificationService.register : GCM Device registration error : ' + err);
					});

					_self.pushService.on('notification', function(notification){
						$log.log(notification);

						if (ionic.Platform.isAndroid()) {
							_self.handleAndroid(notification);
						}
						else if (ionic.Platform.isIOS()) {
							_self.handleIOS(notification);
						}
					});

					deferred.resolve();

					return deferred.promise;
				};


				_self.handleAndroid = function (notification) {
					$log.info('NotificationService.handleAndroid : NOTIFICATION Received');
					$log.debug('NotificationService.handleAndroid : In foreground ' + notification.additionalData.foreground + ' Coldstart ' + notification.additionalData.coldstart);

					$log.info('NotificationService.handleAndroid : ', notification);

					$rootScope.notification = notification;

					switch (notification.additionalData.custom.context) {
						case 'street.reservation.demande':
							$http.get(APIBaseURL + '/spots/' + notification.additionalData.custom.spot)
									.then(function successCallback(response) {
										$log.debug('NotificationService.handleAndroid : ', response.data.spot);

										$rootScope.notification.spot = response.data.spot;
										$rootScope.notification.user = notification.additionalData.custom.demander;
										$rootScope.notification.date = new Date();
										$rootScope.notification.rejected = false;
										$rootScope.notification.accepted = false;
										$rootScope.notification.expired = false;
										$rootScope.waitingPerson = true;

										$timeout(function(){
											$rootScope.map.refreshLayout();
										}, 500);

										/*$rootScope.demande = {};
										$rootScope.demande.spot = response.data.spot;
										$rootScope.demande.user = response.data.spot.user;*/

										$rootScope.notification.userRating = {
											iconOn: 'ion-ios-star',
											iconOff: 'ion-ios-star-outline',
											iconOnColor: 'rgb(0, 0, 0)',
											iconOffColor: 'rgb(0, 0, 0)',
											rating: Math.round(notification.additionalData.custom.demander.street_nb_etoiles * 2) / 2,
											minRating: 0,
											readOnly:true
										};

										$cordovaDialogs.beep(1);

										if(!$rootScope.modalDemande || !$rootScope.modalDemande.isShown()) {
											$ionicModal.fromTemplateUrl('street/proposition/modal-demande-reservation.html', {
												id: 'modalDemande',
												scope: $rootScope,
												animation: 'slide-in-up',
												backdropClickToClose: false
											}).then(function (modal) {
												$rootScope.modalDemande = modal;
												$rootScope.map.setClickable(false);
												$rootScope.modalDemande.show();

												_self.pushService.clearAllNotifications(function() {
													console.log('Push clear all : success');
												}, function() {
													console.log('Push clear all : error');
												});
											});
										}
									}, function errorCallback(error) {
										$log.error('NotificationService.handleAndroid : Erreur lors de la récupération du spot');
									});
							break;
						default:
							$log.warn('NotificationService.handleAndroid : aucun contexte reconnu');
							break;
					}
				};


					// IOS Notification Received Handler
				_self.handleIOS = function(notification) {
					// The app was already open but we'll still show the alert and sound the tone received this way. If you didn't check
					// for foreground here it would make a sound twice, once when received in background and upon opening it from clicking
					// the notification when this code runs (weird).
					if (notification.foreground === '1') {
						// Play custom audio if a sound specified.
						if (notification.sound) {
							var mediaSrc = $cordovaMedia.newMedia(notification.sound);
							mediaSrc.promise.then($cordovaMedia.play(mediaSrc.media));
						}

						if (notification.body && notification.messageFrom) {
							$cordovaDialogs.alert(notification.body, notification.messageFrom);
						}
						else {$cordovaDialogs.alert(notification.alert, 'Push Notification Received');}

						if (notification.badge) {
							PushNotification.setBadgeNumber(notification.badge).then(function (result) {
								$log.debug('NotificationService.handleIOS :Set badge success ' + result);
							}, function (err) {
								$log.warn('NotificationService.handleIOS : Set badge error ' + err);
							});
						}
					}
					// Otherwise it was received in the background and reopened from the push notification. Badge is automatically cleared
					// in this case. You probably wouldn't be displaying anything at this point, this is here to show that you can process
					// the data in this situation.
					else {
						if (notification.body && notification.messageFrom) {
							$cordovaDialogs.alert(notification.body, '(RECEIVED WHEN APP IN BACKGROUND) ' + notification.messageFrom);
						}
						else {$cordovaDialogs.alert(notification.alert, '(RECEIVED WHEN APP IN BACKGROUND) Push Notification Received');}
					}
				};


					// Stores the device token in a db using node-pushserver (running locally in this case)
					// type:  Platform type (ios, android etc)
				_self.storeDeviceToken = function (type) {
					var deferred = $q.defer();
					// data to send
					var data = {
						device: {
							user: $rootScope.currentUser.id,
							deviceModel: $cordovaDevice.getModel(),
							deviceUUID: $cordovaDevice.getUUID(),
							deviceOSVersion: $cordovaDevice.getVersion(),
							type: type,
							token: _self.regId
						}
					};
					$log.debug('NotificationService.storeDeviceToken : Post token for registered device with data ' + JSON.stringify(data));

					$http.post(APIBaseURL + '/devices', data)
							.then(function (data, status) {
								$log.debug('Token stored, device is successfully subscribed to receive push notifications.');
								deferred.resolve();
							}, function (data, status) {
								$log.warn('NotificationService.storeDeviceToken : Error storing device token.' + data + ' ' + status);
								deferred.reject();
							})
							.catch(function(){
								deferred.reject();
							});

					return deferred.promise;
				};


				_self.removeDeviceToken = function () {
					var deferred = $q.defer();

					$http.delete(APIBaseURL + '/devices', {'token': $localStorage.get('push_regId')})
							.success(function (data, status) {
								$log.debug('NotificationService.removeDeviceToken : Token removed, device is successfully unsubscribed and will not receive push notifications.');
								deferred.resolve(data);
							})
							.error(function (data, status) {
										$log.warn('NotificationService.removeDeviceToken : Error removing device token.' + data + ' ' + status);
										deferred.reject('Error removing device token.');
									}
							);

					return deferred.promise;
				};
			}
		]);
