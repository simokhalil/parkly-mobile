/**
 * Created by kHaLiL on 11/11/2016.
 * Retourne le temps restant jusqu'au dateTime reçu en entrée
 */
var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.filters = parkly.modules.filters || angular.module('parkly.filters', []);

parkly.modules.filters
		.filter('nowUntilTime', ['$localStorage', function($localStorage) {
			return function (time) {
				if(typeof time === 'undefined' || !time|| time === 'undefined'){
					return;
				}
				var t = moment(time * 1000).diff(moment());
				return moment(t).format('mm:ss');
			};
		}]);