/**
 * Created by kHaLiL on 11/11/2016.
 */
var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.filters = parkly.modules.filters || angular.module('parkly.filters', []);

parkly.modules.filters
		.filter('formatColor', ['$localStorage', function($localStorage) {
			return function (input) {
				var formattedColors = $localStorage.getObject('couleursFormatted', null);
				if(!formattedColors){
					return input;
				}
				return formattedColors[input];
			};
		}]);