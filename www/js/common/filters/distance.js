/**
 * Created by kHaLiL on 02/04/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.filters = parkly.modules.filters || angular.module('parkly.filters', []);

parkly.modules.filters
		.filter('distance', function () {
			return function (input) {
				input = parseFloat(input);
				if (input >= 1) {
					return (input).toFixed(1) + 'km';
				} else {
					return (input*1000).toFixed(0) + 'm';
				}
			};
		});