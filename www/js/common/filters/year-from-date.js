/**
 * Created by kHaLiL on 11/11/2016.
 */
var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.filters = parkly.modules.filters || angular.module('parkly.filters', []);

parkly.modules.filters
		.filter('yearFromDate', function () {
			return function (dateTime) {
				return moment(dateTime).format('YYYY');
			};
		});