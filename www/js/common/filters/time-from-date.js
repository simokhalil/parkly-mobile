/**
 * Created by kHaLiL on 11/11/2016.
 * Retourne le temps au format HH:mm à partir du dateTime en entrée
 */
var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.filters = parkly.modules.filters || angular.module('parkly.filters', []);

parkly.modules.filters
		.filter('timeFromDate', ['$localStorage', function($localStorage) {
			return function (time) {
				var offset = $localStorage.getInt('time-offset', 0);
				return moment((time - offset) * 1000).format('HH:mm');
			};
		}]);