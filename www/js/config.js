var parkly = parkly || {};
parkly.modules = parkly.modules || {};

parkly.modules.app = parkly.modules.app || {};

parkly.modules.app
		.config(['$stateProvider', '$urlRouterProvider', '$authProvider', '$compileProvider', '$ionicConfigProvider', 'APIBaseURL',
			'$sceDelegateProvider',
			function ($stateProvider, $urlRouterProvider, $authProvider, $compileProvider, $ionicConfigProvider, APIBaseURL,
						  $sceDelegateProvider) {
				$authProvider.loginUrl = APIBaseURL + '/authenticate';
				$compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|file|blob|cdvfile):|data:image\//); // Autoriser les liens vers des images
				$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/); // Autoriser les liens vers des images
				$ionicConfigProvider.scrolling.jsScrolling(false); // Activer le native scroll (désactiver le scroll basé sur JS
				$ionicConfigProvider.backButton.text('').icon('ion-android-arrow-back').previousTitleText(false);
				$sceDelegateProvider.resourceUrlWhitelist(['**']);

				$stateProvider
						.state('startup', {
							url: '/startup',
							templateUrl: 'startup.html',
							controller: 'StartupController',
							params:{
								'allowBack': false
							}
						})

						.state('welcome', {
							url: '/welcome',
							templateUrl: 'auth/welcome.html',
							controller: 'WelcomeController',
							params:{
								'allowBack': false
							}
						})

						.state('user-car', {
							url: '/user-car',
							templateUrl: 'auth/user-car.html',
							controller: 'UserCarController',
							params:{
								'allowBack': false,
								'disableBackButton': false
							}
						})

						.state('login', {
							url: '/login/:context',
							templateUrl: 'auth/login.html',
							controller: 'LoginController',
							params:{
								'allowBack': true,
								'disableBackButton': false
							}
						})

						.state('password', {
							url: '/password',
							templateUrl: 'auth/password-reset.html',
							controller: 'PasswordController',
							params:{
								'allowBack': true
							}
						})

						.state('app', {
							url: '/app',
							abstract: true,
							templateUrl: 'menu.html',
							controller: 'AppController'
						})

						.state('app.je-cherche', {
							url: '/je-cherche',
							views: {
								'menuContent': {
									templateUrl: 'street/recherche/je-cherche.html',
									controller: 'StreetRechercheController'
								}
							},
							params:{
								'allowBack': false
							}
						})

						.state('app.demandesReceived', {
							url: '/demandesReceived',
							views: {
								'menuContent': {
									templateUrl: 'street/notifications/demandes-received.html',
									controller: 'StreetDemandesReceivedController'
								}
							},
							params:{}
						})

						.state('app.demandesSent', {
							url: '/demandesSent',
							views: {
								'menuContent': {
									templateUrl: 'street/historique/demandes-sent.html',
									controller: 'StreetDemandesSentController'
								}
							},
							params:{}
						})

						.state('app.profile', {
							url: '/profile',
							views: {
								'menuContent': {
									templateUrl: 'profile/profile.html',
									controller: 'ProfileController'
								}
							},
							params:{}
						})

						.state('app.parameters', {
							url: '/parameters',
							views: {
								'menuContent': {
									templateUrl: 'parameters/parameters.html',
									controller: 'ParametersController'
								}
							},
							params:{}
						})

						.state('app.about', {
							url: '/about',
							views: {
								'menuContent': {
									templateUrl: 'parameters/about.html',
									controller: 'AboutController'
								}
							},
							params:{
								'allowBack': true
							}
						})

						.state('app.paramsNotifications', {
							url: '/params-notifications',
							views: {
								'menuContent': {
									templateUrl: 'parameters/params-notifications.html',
									controller: 'ParamsNotificationsController'
								}
							},
							params:{
								'allowBack': true
							}
						})

						.state('app.userComment', {
							url: '/user-comment',
							views: {
								'menuContent': {
									templateUrl: 'parameters/user-comment.html',
									controller: 'UserCommentController'
								}
							},
							params:{
								'allowBack': true
							}
						})

						.state('app.browser', {
							url: '/browser',
							views: {
								'menuContent': {
									templateUrl: 'browser.html',
									controller: 'BrowserController'
								}
							},
							params: {
								'url': null,
								'title': null,
								'allowBack': true
							}
						});

				// if none of the above states are matched, use this as the fallback
				$urlRouterProvider.otherwise('/startup');
			}
		]);
