/**
 * Created by kHaLiL on 12/03/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.directives = parkly.modules.directives || angular.module('parkly.directives', []);

parkly.modules.directives
		.directive('headerShrink', ['$document', '$log', function($document, $log) {
			return {
				restrict: 'A',
				link: function($scope, $element, $attr) {
					var resizeFactor, scrollFactor, blurFactor;
					var header = $document[0].body.querySelector('.about-header');
					$scope.$on('userDetailContent.scroll', function(event,scrollView) {
						if (scrollView.__scrollTop >= 0) {
							scrollFactor = scrollView.__scrollTop*1.5;
							header.style[ionic.CSS.TRANSFORM] = 'translate3d(0, +' + scrollFactor + 'px, 0)';
						} else if (scrollView.__scrollTop > -70) {
							resizeFactor = -scrollView.__scrollTop/100 + 0.99;
							// blurFactor = -scrollView.__scrollTop/50;
							header.style[ionic.CSS.TRANSFORM] = 'scale('+resizeFactor+','+resizeFactor+')';
							// header.style.webkitFilter = 'blur('+blurFactor+'px)';
						}
					});
				}
			};
		}]);
