/**
 * Created by kHaLiL on 07/04/2016.
 */

var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.services = parkly.modules.services || angular.module('parkly.services', []);

// Création du service
parkly.modules.services
		.service('PropositionService', ['$http', '$q', 'APIBaseURL', function ($http, $q, APIBaseURL) {
			var _self = this;

			_self.formatted_address = null;
			_self.numRue = null;
			_self.route = null;
			_self.ville = null;
			_self.codePostal = null;
			_self.departement = null;
			_self.region = null;
			_self.pays = null;

			_self.geocodeCoords = function (coords) {

				var deferred = $q.defer();

				$http({
					method: 'GET',
					url: APIBaseURL + '/geocode?latlng=' + coords
				}).then(function successCallback(response) {
					var place = response.data.results[0];

					if(!place){
						_self.formatted_address = 'Lieu inconnu';
					}else{
						_self.formatted_address = place.formatted_address;

						for (var i = 0; i < place.address_components.length; i++) {
							var addressType = place.address_components[i].types[0];

							if (addressType === 'street_number') {
								_self.numRue = place.address_components[i].long_name;
							}
							else if (addressType === 'route') {
								_self.route = place.address_components[i].long_name;
							}
							else if (addressType === 'locality') {
								_self.ville = place.address_components[i].long_name;
							}
							else if (addressType === 'postal_code') {
								_self.codePostal = place.address_components[i].short_name;
							}
							else if (addressType === 'administrative_area_level_2') {
								_self.departement = place.address_components[i].short_name;
							}
							else if (addressType === 'administrative_area_level_1') {
								_self.region = place.address_components[i].short_name;
							}
							else if (addressType === 'country') {
								_self.pays = place.address_components[i].short_name;
							}
						}
					}

					deferred.resolve(response.data.results[0]);
				}, function errorCallback(error) {
					deferred.reject(error);
				});

				return deferred.promise;
			};

		}]);