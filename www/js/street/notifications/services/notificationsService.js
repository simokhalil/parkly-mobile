/**
 * Created by kHaLiL on 07/04/2016.
 */

var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.services = parkly.modules.services || angular.module('parkly.services', []);

// Création du service
parkly.modules.services
		.service('NotificationsService', ['$http', '$q', 'APIBaseURL', '$log', function ($http, $q, APIBaseURL, $log) {
			var _self = this;

			//_self.demandes = [];

			_self.getDemandes = function () {
				var deferred = $q.defer();

				$http.get(APIBaseURL + '/spots/demandes/received').then(function(response){
					$log.debug('$get demandes data : ', response);
					deferred.resolve(response.data.demandes);
				}, function(){
					deferred.reject('Un problème est survenu lors de la récupération des demandes reçues');
				});

				return deferred.promise;
			};

			_self.refresh = function(){
				return $http.get(APIBaseURL + '/spots/demandes/received');
			};

		}]);