/**
 * Created by kHaLiL on 13/03/2016.
 */
var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.services = parkly.modules.services || angular.module('parkly.services', []);

parkly.modules.services
		.factory('SpotsDelta', function() {
			return {

				/**
				 * Creates a map out of an array be choosing what property to key by
				 * @param {object[]} array Array that will be converted into a map
				 * @param {string} prop Name of property to key by
				 * @return {object} The mapped array. Example:
				 *     mapFromArray([{a:1,b:2}, {a:3,b:4}], 'a')
				 *     returns {1: {a:1,b:2}, 3: {a:3,b:4}}
				 */
				mapFromArray : function(array, prop) {
					var map = {};
					for (var i=0; i < array.length; i++) {
						if(typeof array[i] !== 'undefined') {
							map[array[i][prop]] = array[i];
						} else{
							// On rectifie le lenght du tableau
							if(array.length > 0) {array.length--;}
						}
					}
					return map;
				},

				isEqual : function (a, b) {
					//return false;
					return parseInt(a.user_id) === parseInt(b.user_id) &&
							parseFloat(a.lat) === parseFloat(b.lat) &&
							parseFloat(a.lng) === parseFloat(b.lng) &&
							parseInt(a.end) === parseInt(b.end);
				},

				/**
				 * @param {object[]} oldArray old array of objects
				 * @param {object[]} newArray new array of objects
				 * @param {object} comparator An object with changes
				 */
				getDelta : function(oldArray, newArray, comparator)  {
					var delta = {
						added: {},
						deleted: {},
						changed: {}
					};
					var mapOld = this.mapFromArray(oldArray, 'id');
					var mapNew = this.mapFromArray(newArray, 'id');
					for (var id in mapOld) {
						if (!mapNew.hasOwnProperty(id)) {
							delta.deleted[id] = (mapOld[id]);
						} else if (!comparator(mapNew[id], mapOld[id])){
							delta.changed[id] = (mapNew[id]);
						}
					}

					for (var id in mapNew) {
						if (!mapOld.hasOwnProperty(id)) {
							delta.added[id] = ( mapNew[id] );
						}
					}
					return delta;
				}
			};
		});
