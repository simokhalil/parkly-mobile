/**
 * Created by kHaLiL on 07/04/2016.
 */

var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.services = parkly.modules.services || angular.module('parkly.services', []);

// Création du service
parkly.modules.services
		.service('MapsService', ['$http', '$q', '$window', function ($http, $q, $window) {
			var _self = this;

			//_self.demandes = [];

			_self.mapsApiLoaded = false;

			_self.loadMapsAPI = function () {
				if(_self.mapsApiLoaded) {return;}

				var deferred = $q.defer();
				var script = document.createElement('script');
				
				$window.initMap = function() {
					//console.log("Map  init ");
					_self.mapsApiLoaded = true;
					deferred.resolve();
				};
				script.src = 'http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDOuhKNEhIwb1_bQ_nphcOBoXawkbDxDJg';
				document.body.appendChild(script);
				return deferred.promise;
			};

		}]);