/**
 * Created by kHaLiL on 19/03/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.controllers = parkly.modules.controllers || angular.module('parkly.controllers', []);

// Création du controleur pour la vue Login
parkly.modules.controllers
		.controller('StreetRechercheController', ['$scope', '$ionicHistory', '$http', '$ionicModal', '$log', '$rootScope', 'APIBaseURL',
			'geoLocation', '$localStorage', '$ionicLoading', '$cordovaDialogs', 'SpotService', 'PropositionService', '$interval',
			'$ionicPlatform', '$timeout', 'SpotsDelta', '$q', 'MapsService',
			function ($scope, $ionicHistory, $http, $ionicModal, $log, $rootScope, APIBaseURL, geoLocation, $localStorage, $ionicLoading,
					  $cordovaDialogs, SpotService, PropositionService, $interval, $ionicPlatform, $timeout, SpotsDelta, $q, MapsService) {
			
				var _self = this;

				// Attributs Recherche
				_self.posOptions = {maximumAge: 3000, timeout: 10000, enableHighAccuracy: false};
				$scope.showSpotSlider = false;
				$scope.geolocating = false;
				$rootScope.spotEnCours = $localStorage.getObject('_st_spot', null);
				$scope.spots = [];
				$rootScope.reservationEnCours = null;
				$scope.showReservationEnCours = false;
				$scope.parkingsPublics = [];

				// Attributs Proposition
				$scope.showSpotProposition = false;
				$scope.hideSpotPropositionButton = false;
				$scope.localisation = {};

				$scope.parkingPayant = {
					value: 'false'
				};
				$scope.coords = {};
				// Données du timer (label + pourcentage)
				$scope.progress = {
					max: null,
					current: null
				};

				// Modal de détails d'un spot
				$ionicModal.fromTemplateUrl('street/recherche/modal-spot.html', function ($ionicModal) {
					$scope.spotModal = $ionicModal;
				}, {
					id: 'spotModal',
					scope: $scope,
					animation: 'slide-in-up'
				});

				$scope.$on('$ionicView.beforeEnter', function(){
					$rootScope.viewTitle = null;

					// Si déjà garé => Déclenchement Timer
					if ($scope.spotEnCours) {
						$rootScope.spotEnCours = JSON.parse($localStorage.getObject('_st_spot', null));
						$scope.spotEnCoursEndTime = $localStorage.getInt('_st', null);
						$scope.timeToLeave = Math.floor(($scope.spotEnCoursEndTime - (new Date())) / 1000);

						// Données du timer (label + pourcentage)
						$scope.progress.max = $localStorage.getInt('_st_ttl', $scope.timeToLeave);
						$scope.progress.current = _self.timeRemaining;

						if(!_self.interval) {
							_self.interval = $interval(_self.countdownFunction, 1000);
						}
					} else if (_self.interval) {
						$interval.cancel(_self.interval);
						_self.interval = null;
					}
				});

				$scope.$on('$ionicView.afterEnter', function() {
					$rootScope.startGetSpots();
					//_self.animateCamera(geoLocation.getGeolocation().lat, geoLocation.getGeolocation().lng);
					_self.geolocate(false);
					_self.watch = $interval(_self.geolocate, 5000);

					$ionicHistory.clearHistory();
				});

				_self.geolocate = function(fix){
					//fix = fix || null;

					if(!$scope.geolocating) {
						$scope.geolocating = true;
						navigator.geolocation.getCurrentPosition(function (position) {
							geoLocation.setGeolocation(position.coords.latitude, position.coords.longitude);
							if(!fix){
								_self.animateCamera(position.coords.latitude, position.coords.longitude);
							}
							// broadcast this event on the rootScope
							$rootScope.$broadcast('location:change', geoLocation.getGeolocation());
							$rootScope.geolocationAvailable = true;
							$scope.geolocating = false;
						}, function (err) {
							$rootScope.$broadcast('location:error', geoLocation.getGeolocation());
							$log.warn('Event : location:error', err);
							$rootScope.geolocationAvailable = false;
							$scope.geolocating = false;
						}, _self.posOptions);
					}
				};

				/**
				 * Initialisation du slider (swiper)
				 * @type {{value: number, options: {floor: number, ceil: number, step: number, showTicks: boolean, showSelectionBar: boolean, showTicksValues: boolean, getSelectionBarColor: $scope.rzSlider.options.getSelectionBarColor, getPointerColor: $scope.rzSlider.options.getPointerColor, translate: $scope.rzSlider.options.translate}}}
				 */
				$scope.rzSlider = {
					value: 3,
					options : {
						floor: 2,
						ceil: 5,
						step: 1,
						showTicks: true,
						showSelectionBar: true,
						showTicksValues: true,
						getSelectionBarColor: function(value) {
							return '#c14407';
						},
						getPointerColor: function(value) {
							return '#f1611a';
						},
						translate: function(value, sliderId, label) {
							return value+'min';
						}
					}
				};

				/**
				 * Fonction à exécuter au chargement de la map
				 */
				_self.mapReadyFunction = function () {
					$log.debug('Google Maps ; Map initialisée');
					// Affiche la localisation du user sur la carte (point bleu)
					$rootScope.map.setMyLocationEnabled(true);
					// Réinitialise la carte
					$rootScope.map.clear();
					// Ajoute le marker du user s'il est déjà garé
					if($scope.spotEnCours){
						_self.makeUserSpotMarker($rootScope.spotEnCours.lat, $rootScope.spotEnCours.lng);
					}
					// Localise l'utilisateur et pointe sur la position
					$scope.goToMyLocation();

					if(!$localStorage.getInt('tuto_1_done', 0)){
						$ionicModal.fromTemplateUrl('tuto/tuto_modal_1.html', {
							scope: $scope,
							animation: 'slide-in-up',
							backdropClickToClose: true,
							hardwareBackButtonClose: true,
							id: 'modalTuto',
						}).then(function(modal) {
							$scope.modalTuto = modal;
							$rootScope.map.setClickable(false);
							$scope.modalTuto.show();
							//$localStorage.set('tuto_1_done', 1);
						});
					}
				};

				/**
				 * Fonction à exécuter au mouvement de la carte
				 * @param position
				 */
				_self.cameraChangeFunction = function (position) {

					//$log.debug('Google Maps : camera changed');
					$rootScope.map.getCameraPosition(function (camera) {
						$scope.coords.lat = camera.target.lat;
						$scope.coords.lng = camera.target.lng;
						$scope.coords.formatted = (camera.target.lat) + ',' + camera.target.lng;
						//$log.log('Google Maps : Coords => ', $scope.coords);
						$scope.geocodeAdresse($scope.coords.formatted);
						$timeout.cancel($scope.cameraChangeIdle);
						$scope.cameraChangeIdle = $timeout(function(){
							$scope.getSpots();
							$scope.getParkingsPublics();
						}, 1000);
					});

				};

				/**
				 * Fonctions a exécuter au clic sur la map : masquer le slider du parker
				 * Pour activer le masquage au clic -> $rootScope.clickMapFunction = $rootScope.clickMapFunctionActive();
				 * Pour désactiver le masquage au clic -> $rootScope.clickMapFunction = $rootScope.clickMapFunctionInactive();
				 * @param position
				 */
				// Clic actif
				_self.clickMapFunction = function (position) {
					$log.debug('Google Maps : Map clicked', position.toUrlValue());
					if ($scope.showSpotSlider) {
						_self.hideParkerCard();
					}
				};

				// Function that return a LatLng Object to Map
				$scope.setPosition = function(lat, lng) {
					return new plugin.google.maps.LatLng(lat, lng);
				};

				$rootScope.startGetSpots = function(){
					$interval.cancel($rootScope.getSpotsInterval);
					$interval.cancel($rootScope.getParkingsPublicsInterval);
					$rootScope.getSpotsInterval = null;
					$rootScope.getParkingsPublicsInterval = null;
					$rootScope.getSpotsInterval = $interval(function () {
						$scope.getSpots();
					}, 10000);
					$rootScope.getParkingsPublicsInterval = $interval(function () {
						$scope.getParkingsPublics();
					}, 60000);
				};

				$rootScope.stopGetSpots = function(){
					if($rootScope.getSpotsInterval) {
						$interval.cancel($rootScope.getSpotsInterval);
						$rootScope.getSpotsInterval = null;
					}
					$scope.spots = [];

					if($rootScope.getParkingsPublicsInterval) {
						$interval.cancel($rootScope.getParkingsPublicsInterval);
						$rootScope.getParkingsPublicsInterval = null;
					}
					$scope.parkingsPublics = [];
				};

				$scope.initialSliderIndex = 0;
				$scope.spotSliderOptions = {
					'loop': false,
					'nextButton': '.swiper-button-next',
					'prevButton': '.swiper-button-prev',
					//'centeredSlides' : true,
					'slidesPerView' : 'auto',
					'initialSlide' : $scope.initialSliderIndex,
					//'spaceBetween' : 30,
					'pagination' : '.custom-swiper-pagination',
					onInit: function(slider){
						$log.debug('Slider initialized !', slider);
						$scope.spotSlider = slider;
						$scope.spotSlider.slideTo($scope.initialSliderIndex);
					},
					onSlideChangeEnd: function(slider){
						$log.debug('StreetRechercheController : Slider => The active index is ' + slider.activeIndex);
						_self.animateCamera($scope.spots[slider.activeIndex].lat, $scope.spots[slider.activeIndex].lng);
					}
				};


				/*************************************************************************
				 * View Events
				 ************************************************************************/

				$scope.$on('$ionicView.beforeLeave', function () {
					$log.debug('StreetRechercheController : leaving');
					if ($rootScope.getSpotsInterval) {
						$rootScope.stopGetSpots();
						$log.debug('StreetRechercheController : getSpotsInterval arrêté');
						$interval.cancel(_self.watch);
						_self.watch = null;
						$log.debug('StreetRechercheController : watchPosition arrêté');
					}
				});

				$scope.$on('spotModal.hidden', function () {
					$rootScope.map.setClickable(true);
					if (_self.SpotModalinterval) {
						$interval.cancel(_self.SpotModalinterval);
						_self.SpotModalinterval = null;
					}
					$log.log('interval cleared');
				});

				// Cleanup the modal when we're done with it!
				$scope.$on('$destroy', function () {
					$scope.spotModal.remove();
				});

				/***********************************************************
				 * Controller Utils
				 **********************************************************/

				/**
				 * Retourne le texte d'un marker (nb de minutes/heures) à partir du timestamp de fin d'un spot
				 * @param end
				 * @returns {string}
				 */
				_self.markerRemainingTime = function (end) {
					var offset = $localStorage.getInt('time-offset', 0);
					var nbMinutes = moment((end - offset)* 1000).diff(moment(), 'minutes');
					//return nbMinutes < 60 ? nbMinutes + ' <span style="font-size:13px">min</span>' : (Math.floor(nbMinutes / 60) + ' h');
					//return nbMinutes < 60 ? nbMinutes + ' min' : (Math.floor(nbMinutes / 60) + ' h');
					return (nbMinutes < 60 ? nbMinutes : (Math.floor(nbMinutes / 60))).toString();
				};

				/**
				 * Déplace la caméra avec animation
				 * @param lat
				 * @param lng
				 * @param zoom
				 * @param duration
				 */
				_self.animateCamera = function(lat, lng, zoom, duration){
					//$log.debug('animateCamra : ', lat, lng);
					zoom = zoom || 17;
					duration = duration || 300;

					$rootScope.map.animateCamera({
						'target': $scope.setPosition(lat, lng),
						'zoom': zoom,
						'duration': duration
					});
				};

				$scope.openPropositionWindowNew = function(){
					if(typeof $rootScope.map !== 'undefined') {
						_self.animateCamera($scope.coords.lat, $scope.coords.lng);
					}
					$scope.showSpotPropositionNew = true;
					$timeout(function(){
						$rootScope.map.refreshLayout();
					}, 500);
				};

				/**
				 * Affiche la fenêtre de proposition de spot
				 */
				$scope.openPropositionWindow = function(){
					$scope.showSpotSlider = false;
					//$scope.spotSlider.update();
					$scope.showSpotProposition = true;
					if(typeof $rootScope.map !== 'undefined') {
						_self.animateCamera($scope.coords.lat, $scope.coords.lng);
					}
					$scope.hideSpotPropositionButton = true;

					if(!$localStorage.getInt('tuto_2_done', 0)){
						$ionicModal.fromTemplateUrl('tuto/tuto_modal_2.html', {
							scope: $scope,
							animation: 'slide-in-up',
							backdropClickToClose: true,
							hardwareBackButtonClose: true,
							id: 'modalTuto',
						}).then(function(modal) {
							$scope.modalTuto = modal;
							$rootScope.map.setClickable(false);
							$scope.modalTuto.show();
							//$localStorage.set('tuto_2_done', 1);
						});
					}
				};
				/**
				 * Ferme la fenêtre de proposition de spot
				 */
				$scope.closePropositionWindowNew = function(){
					$scope.showSpotPropositionNew = false;
					$rootScope.map.setClickable(true);
					_self.animateCamera($scope.coords.lat, $scope.coords.lng);
					$timeout(function(){
						$rootScope.map.refreshLayout();
					}, 500);
				};
				$scope.closePropositionWindow = function(){
					$scope.showSpotProposition = false;
					$scope.hideSpotPropositionButton = false;
					$rootScope.map.setClickable(true);
					_self.animateCamera($scope.coords.lat, $scope.coords.lng);
				};

				/**
				 *  Obtention de la localisation actuelle de l'utilisateur
				 */
				$scope.goToMyLocation = function () {
					_self.animateCamera((geoLocation.getGeolocation().lat), geoLocation.getGeolocation().lng);
				};


				//$scope.$on('$ionicView.enter', function(){
				// Init GMaps native plugin
				if(typeof cordova !== 'undefined') {
					$ionicPlatform.ready(function () {
						$log.debug('Google Maps : Initialisation...');
						// Getting the map selector in DOM
						var div = document.getElementById('map_canvas');

						// Invoking Map using Google Map SDK v2 by dubcanada
						$rootScope.map = plugin.google.maps.Map.getMap(div, {
							'controls': {
								'compass': false,
								'myLocationButton': false,
								'indoorPicker': false,
								'zoom': false
							},
							'gestures': {
								'scroll': true,
								'tilt': false,
								'rotate': false,
								'zoom': true
							},
							'camera': {
								'latLng': $scope.setPosition(geoLocation.getGeolocation().lat, geoLocation.getGeolocation().lng),
								'zoom': 16
							}
						});

						/**
						 * Listeners sur la carte Maps
						 */
						// Listener sur le chargement de la map (quand elle est prête)
						$rootScope.map.addEventListener(plugin.google.maps.event.MAP_READY, _self.mapReadyFunction);
						// Listener sur le mouvement de la map
						$rootScope.map.addEventListener(plugin.google.maps.event.CAMERA_CHANGE, _self.cameraChangeFunction);
						//Listener sur le clic sur la map
						$rootScope.map.addEventListener(plugin.google.maps.event.MAP_CLICK, _self.clickMapFunction);
					});
				}
				//});

				/**
				 * Récupérer les parkings publics
				 */
				$scope.getParkingsPublics = function(){
					if($rootScope.networkAvailable && $scope.coords && $rootScope.localisation) {
						SpotService.getParkingsPublics($scope.coords.lat, $scope.coords.lng, $rootScope.localisation.ville, $rootScope.localisation.departement, $rootScope.localisation.region).then(function(response){
							console.log(response.parkingsPublics);
							var delta = SpotsDelta.getDelta($scope.parkingsPublics, response.parkingsPublics, SpotsDelta.isEqual);

							for (var i = 0; i < $scope.parkingsPublics.length; i++) {
								var parkingAtraiter = $scope.parkingsPublics[i];

								if (parkingAtraiter && delta.deleted.hasOwnProperty(parkingAtraiter.id)) {
									parkingAtraiter.marker.remove();
									delete $scope.parkingsPublics[i];
								} else if (parkingAtraiter && delta.changed.hasOwnProperty(parkingAtraiter.id)) {
									var markerTemp = parkingAtraiter.marker;
									$scope.parkingsPublics[i] = delta.changed[parkingAtraiter.id];
									$scope.parkingsPublics[i].marker = markerTemp;

									var color = _self.getParkingPublicMarkerColor(parkingAtraiter.nbPlacesDispo);

									$scope.parkingsPublics[i].marker.setIcon({
										url: './img/markers/icn_md_marker_parking_'+color+'.png',
										size: {
											width: 35,
											height: 40
										}
									});
								}
							}

							// Ajout des nouveaux spots
							for (var newSpot in delta.added) {
								i = i++;

								$scope.parkingsPublics[i] = (delta.added[newSpot]);

								var color = _self.getParkingPublicMarkerColor(delta.added[newSpot].nbPlacesDispo);

								$rootScope.map.addMarker({
									position: $scope.setPosition(delta.added[newSpot].lat, delta.added[newSpot].lng),
									icon: {
										url: './img/markers/icn_md_marker_parking_'+color+'.png',
										size: {
											width: 35,
											height: 40
										}
									},
									markerClick: function () {
										_self.animateCamera(this.get('lat'), this.get('lng'), null, null);
									},
									spotIndex: i,
									lat: delta.added[newSpot].lat,
									lng: delta.added[newSpot].lng,
									title: delta.added[newSpot].nom,
									snippet: 'Places disponibles : ' + delta.added[newSpot].nbPlacesDispo + (delta.added[newSpot].nbPlaces ? ('/' + delta.added[newSpot].nbPlaces) : ''),
									styles : {
										'text-align': 'center',
										'font-style': 'italic',
										'font-weight': 'bold',
										'color': color
									}
								}, function (marker) {
									//$log.debug('$rootScope.addMarker ', marker);
									$scope.parkingsPublics[i].marker = marker;
								});
							}
						});
					}
				};

				_self.getParkingPublicMarkerColor = function(nb){
					var placesDispo = parseInt(nb);
					var color = 'green';
					if(placesDispo == 0){
						color = 'gray';
					} else if(placesDispo < 10){
						color = 'orange';
					}
					return color;
				};

				/**
				 * Récupérer les spots du serveur
				 */
				$scope.getSpots = function () {
					if($rootScope.networkAvailable && $scope.coords) {
						SpotService.getSpots($scope.coords.lat, $scope.coords.lng).then(function (response) {
							//$log.debug('received spots : ', response);

							//$log.debug('$scope.spots (old) : ', $scope.spots);

							//$log.debug('Calculating delta...');
							var delta = SpotsDelta.getDelta($scope.spots, response.spots, SpotsDelta.isEqual);
							//$log.debug('Delta ', delta);

							// Suppression/Modification des spots obsolètes
							for (var i = 0; i < $scope.spots.length; i++) {
								var spotATraiter = $scope.spots[i];
								//$log.debug('StreetRechercheController.getSpots : spotATraiter : ', spotATraiter);
								if (spotATraiter && delta.deleted.hasOwnProperty(spotATraiter.id)) {
									//$log.debug('StreetRechercheController.getSpots : spot à supprimer !');
									//$log.debug('StreetRechercheController.getSpots : $scope.showSpotSlider = ' + $scope.showSpotSlider + '; $scope.currentSpot = ' + $scope.currentSpot);
									if ($scope.showSpotSlider && $scope.currentSpot.id === spotATraiter.id) {
										//$log.debug('StreetRechercheController.getSpots : spotATraiter affiché en card');
										_self.hideParkerCard();
										//$log.debug('StreetRechercheController.getSpots : spotATraiter affiché en card => masqué');
									}
									spotATraiter.marker.remove();
									//$log.debug('StreetRechercheController.getSpots : spotATraiter à supprimer => marker supprimé !');
									delete $scope.spots[i];
									if ($scope.showSpotSlider) {$scope.spotSlider.update();}
									//$log.debug('StreetRechercheController.getSpots : spotATraiter à supprimer => supprimé !');
								} else if (spotATraiter && delta.changed.hasOwnProperty(spotATraiter.id)) {
									var markerTemp = spotATraiter.marker;
									$scope.spots[i] = delta.changed[spotATraiter.id];
									$scope.spots[i].marker = markerTemp;

									$scope.spots[i].userRating = {
										iconOn: 'ion-ios-star',
										iconOff: 'ion-ios-star-outline',
										iconOnColor: 'rgb(0, 0, 0)',
										iconOffColor: 'rgb(0, 0, 0)',
										rating: Math.round($scope.spots[i].user.street_nb_etoiles * 2) / 2,
										minRating: 0,
										readOnly: true
									};

									if ($scope.showSpotSlider) {$scope.spotSlider.update();}

									//$log.debug('StreetRechercheController : Gmaps Marker => setIcon() ...');
									// On recrée l'image du marker (nouveau texte) et on modifie le marker existant - On envoie i pour le récupérer derrière dans le callback
									_self.createMarkerImage(delta.changed[spotATraiter.id].end, parseInt(spotATraiter.parking_payant), i).then(function (markerIconDataURL) {
										$scope.spots[markerIconDataURL.counter].marker.setIcon({
											'url': markerIconDataURL.canvasDataUrl
										});
									});
									//$log.debug('StreetRechercheController : Gmaps Marker => setIcon() ... done !');
								}
							}

							// Ajout des nouveaux spots
							for (var newSpot in delta.added) {
								i = i++;

								//$log.debug('Ajout de newSpot : ', delta.added[newSpot]);
								$scope.spots[i] = (delta.added[newSpot]);

								$scope.spots[i].userRating = {
									iconOn: 'ion-ios-star',
									iconOff: 'ion-ios-star-outline',
									iconOnColor: 'rgb(0, 0, 0)',
									iconOffColor: 'rgb(0, 0, 0)',
									rating: Math.round($scope.spots[i].user.street_nb_etoiles * 2) / 2,
									minRating: 0,
									readOnly: true
								};

								if ($scope.showSpotSlider) {$scope.spotSlider.update();}

								// Créer l'image du marker puis créer le marker avec l'icone en base64
								_self.createMarkerImage(delta.added[newSpot].end, parseInt(delta.added[newSpot].parking_payant), i).then(function (markerIconDataURL) {
									$rootScope.map.addMarker({
										//'marker': $scope.spots[i].marker,
										'position': $scope.setPosition(delta.added[newSpot].lat, delta.added[newSpot].lng),
										'icon': markerIconDataURL.canvasDataUrl,
										'markerClick': function () {
											var index = this.get('spotIndex');
											$scope.initialSliderIndex = index;
											$log.debug('GMaps : Marker clicked => index ' + index);
											$scope.currentSpot = $scope.spots[index];
											//$log.debug('GMaps : Marker clicked => currentSpot set');
											if (!$scope.showSpotSlider) {
												//$log.debug('GMaps : Marker clicked => $scope.showSpotSlider : false => call _self.showParkerCard()');
												_self.showSpotSlider();
												//$log.debug('GMaps : Marker clicked => $scope.showSpotSlider : false => call _self.showParkerCard() => shown!');
											}
											//$log.debug($scope.spotSlider);
											//$log.debug('GMaps : Marker clicked => Slider slided to index', index);
											//$scope.spotSlider.update();
											//$scope.spotSlider.slideTo(index);
											_self.animateCamera(this.get('lat'), this.get('lng'), null, null);
										},
										'spotIndex': i,
										'lat': delta.added[newSpot].lat,
										'lng': delta.added[newSpot].lng
									}, function (marker) {
										//$log.debug('$rootScope.addMarker ', marker);
										$scope.spots[i].marker = marker;
									});
								});
							}

							//$log.debug('$scope.spots (new) : ', $scope.spots);
						});
					} else{
						// Pas de connexion => Ne rien faire
					}
				};

				/**
				 * Create marker image with text
				 * @param markerEnd Heure de fin de validité du spot
				 * @param payant true/false
				 * @param counter
				 * @returns {Promise}
				 */
				_self.createMarkerImage = function(markerEnd, payant, counter){
					var deferred = $q.defer();

					// Creating the marker with text
					var canvas = document.createElement('canvas');
					//canvas.width = 50;
					canvas.width = 40;
					//canvas.height = 43;
					canvas.height = 46;
					//canvas.style.width=50;//actual width of canvas
					canvas.style.width=40;//actual width of canvas
					//canvas.style.height=43;//actual height of canvas
					canvas.style.height=46;//actual height of canvas
					var context = canvas.getContext('2d');

					var img = new Image();
					//img.src = payant ? './img/street_spot_marker_orange_50x43.png' : './img/street_spot_marker_vert_50x43.png';
					img.src = './img/markers/icn_md_marker_spot.png';
					//$log.debug('StreetRechercheController : Gmaps : Marker image size = ', img.height);
					img.onload = function() {
						context.drawImage(img, 0, 0, canvas.width, canvas.height);

						//context.font = '9.5pt Muli';
						context.font = '18pt Muli';
						context.fillStyle = 'white';
						context.moveTo(25, 0);
						context.textAlign = 'center';
						//context.fillText(_self.markerRemainingTime(parseInt(markerEnd)), 25, 18);
						context.fillText(_self.markerRemainingTime(parseInt(markerEnd)), 20, 30);

						//$log.error('newSpot.end = ', markerEnd);
						//$log.error('parseInt(newSpot.end) = ', parseInt(markerEnd));

						//$log.debug('StreetRechercheController : Gmaps : Marker data url = ', canvas.toDataURL());

						deferred.resolve({canvasDataUrl:canvas.toDataURL(), counter: counter});
					};

					return deferred.promise;
				};

				/**
				 * Create marker image with text for public parkings
				 * @param texte du marker
				 * @param payant true/false
				 * @param counter
				 * @returns {Promise}
				 */
				_self.createParkingPublicMarkerImage = function(text, payant, counter){
					var deferred = $q.defer();

					// Creating the marker with text
					var canvas = document.createElement('canvas');
					canvas.width = 35;
					canvas.height = 45;
					canvas.style.width=35;//actual width of canvas
					canvas.style.height=45;//actual height of canvas
					var context = canvas.getContext('2d');

					var img = new Image();
					img.src = payant ? './img/street_parking_public_marker.png' : './img/street_parking_public_marker.png';
					img.onload = function() {
						context.drawImage(img, 0, 0, canvas.width, canvas.height);

						context.font = '9.5pt Muli';
						context.fillStyle = 'white';
						context.moveTo(25, 0);
						context.textAlign = 'center';
						context.fillText(text, 25, 18);

						deferred.resolve({canvasDataUrl:canvas.toDataURL(), counter: counter});
					};

					return deferred.promise;
				};

				/**
				 * Afficher le détail d'un spot
				 */
				$scope.showSpot = function (spot) {
					if (!spot) {
						return;
					}
					$scope.mapsIframeUrl = 'https://www.google.com/maps/embed/v1/directions?origin='+$scope.coords.lat+',' + $scope.coords.lng+'&destination='+spot.lat+','+spot.lng+'&key=AIzaSyDXlBV7pWFgxuML4h5nrfFTZaSv5bhzUzQ';
					$scope.currentSpot = spot;
					$rootScope.map.setClickable(false);
					$scope.spotModal.show();
					_self.SpotModalinterval = $interval(function () {
						if (moment() >= (spot.end * 1000)) {
							$interval.cancel(_self.SpotModalinterval);
							_self.SpotModalinterval = null;
							$rootScope.map.setClickable(true);
							$scope.spotModal.hide();
							_self.hideParkerCard();
							$scope.getSpots();
						}
					}, 1000);
				};

				/**
				 * Ferme la fenêtre de détails du spot
				 */
				$scope.hideSpot = function(){
					$interval.cancel(_self.SpotModalinterval);
					_self.SpotModalinterval = null;
					$rootScope.map.setClickable(true);
					$scope.spotModal.hide();
					_self.hideParkerCard();
					$scope.getSpots();
					$scope.getParkingsPublics();
				};

				/**
				 * Affiche le spot sélectionné dans la map
				 */
				_self.showSpotSlider = function () {
					if(!$scope.showSpotProposition) {
						$log.debug('StreetRechercheController.showParkerCard() => OK');
						$scope.showSpotSlider = true;
						try {
							$scope.$apply();
						}
						catch(err) {
							$log.warn('Erreur de digest $apply', err);
						}
					} else{
						$log.debug('StreetRechercheController.showParkerCard() => KO');
					}
				};

				/**
				 * Masque le spot sélectionné dans la map
				 */
				_self.hideParkerCard = function () {
					$scope.showSpotSlider = false;
					/*try {
						$scope.$apply();
					}
					catch(err) {
						$log.warn('Erreur de digest $apply', err);
					}*/
				};

				/**
				 * Affiche la réservation en cours
				 */
				_self.showReservationEnCours = function(){
					$scope.showSpotSlider = false;
					//$scope.spotSlider.update();
					$scope.showSpotProposition = false;
					$scope.showReservationEnCours = true;
				};

				/**
				 * Masque la réservation en cours
				 */
				_self.hideReservationEnCours = function(){
					$scope.showReservationEnCours = false;
				};

				/**
				 * Réserver un spot
				 */
				$scope.reserverSpot = function (spot) {
					$ionicLoading.show();
					$http({
						method: 'POST',
						url: APIBaseURL + '/spots/' + spot.id + '/book'
					}).success(function (data) {
						$ionicLoading.hide();
						$rootScope.reservationEnCours = data.reservation;
						$rootScope.reservationEnCours.userRating = {
							iconOn: 'ion-ios-star',
							iconOff: 'ion-ios-star-outline',
							iconOnColor: '#333',
							iconOffColor: '#333',
							rating: Math.round(data.reservation.spot.user.street_nb_etoiles * 2) / 2,
							minRating: 0,
							readOnly:true
						};
						_self.showReservationEnCours();
						$ionicModal.fromTemplateUrl('street/recherche/modal-notification-acceptation.html', {
							id: 'modalAcceptation',
							scope: $rootScope,
							animation: 'slide-in-up',
							backdropClickToClose: false
						}).then(function (modal) {
							$rootScope.modalAcceptation = modal;
							$rootScope.map.setClickable(false);
							$rootScope.modalAcceptation.show();
						}, function(){
							$cordovaDialogs.alert($rootScope.notification.message, $rootScope.notification.title);
						});

						// On incrémente le nombre d'usages
						var current_usages = parseInt($localStorage.get('_u_cur_usages'));
						$localStorage.set('_u_cur_usages', current_usages + 1);
					}).error(function (err) {
						$log.error('Erreur d\'envoi de la demande');
						$log.log(err);
						$cordovaDialogs.alert('Votre demande n\'a pas pu être traitée. Merci de réessayer. Si le problème persiste, merci de nous le signaler', 'Erreur');
						$ionicLoading.hide();
					});
				};

				/**
				 * Valider l'échange (par le bénéficiaire
				 */
				$scope.validerEchangeDemandeur = function(){
					_self.hideReservationEnCours();
				};

				/**
				 * Exécuté au clic sur le bouton de proposition de place
				 */
				$scope.proposerMaPlace = function () {
					if(typeof $scope.map !== 'undefined') {$rootScope.map.setClickable(true);}
					//$scope.openPropositionWindow();
					$scope.openPropositionWindowNew();
				};

				/**
				 * Fonction permettant de déclarer un spot avec position GPS et heure de départ
				 */
				$scope.garer = function () {
					$ionicLoading.show();

					var spot = {
						payant: !JSON.parse($scope.parkingPayant.value),
						latlng: $scope.coords.formatted,
						address: $scope.localisation.adresseCourte,
						numRue: $scope.localisation.numRue,
						rue: $scope.localisation.route,
						ville: $scope.localisation.ville,
						cp: $scope.localisation.cp,
						departement: $scope.localisation.departement,
						region: $scope.localisation.region,
						pays: $scope.localisation.pays,
						endDate: moment().add($scope.rzSlider.value, 'minutes').add(($localStorage.getInt('time-offset', 0)), 'seconds') //new Date($scope.endDate + $scope.endTime)
					};

					if($rootScope.networkAvailable) {
						SpotService.postSpot(spot).then(function successCallback(data) {
							$scope.closePropositionWindowNew();

							// récupérer les données mises en localStorage par SpotService.postSpot()
							$rootScope.spotEnCours = JSON.parse($localStorage.getObject('_st_spot', null));
							$scope.spotEnCoursEndTime = $localStorage.getInt('_st', null);
							$scope.timeToLeave = Math.floor(($scope.spotEnCoursEndTime - (new Date())) / 1000);
							$localStorage.set('_st_ttl', $scope.timeToLeave);
							$scope.progress.max = $scope.timeToLeave;

							$interval.cancel(_self.interval);
							_self.interval = null;
							_self.interval = $interval(_self.countdownFunction, 1000);

							$rootScope.map.clear();
							$rootScope.stopGetSpots();

							_self.makeUserSpotMarker($scope.coords.lat, $scope.coords.lng);

							$ionicLoading.hide();
						}, function errorCallback(error) {
							$ionicLoading.hide();

							$cordovaDialogs.alert('Une erreur technique s\'est produite. Veuillez réessayer ultérieurement', 'Erreur', 'OK');
						});
					} else{
						$ionicLoading.hide();
						$rootScope.Utils.showNetworkErrorMsg();
					}
				};


				/**
				 * Ajoute un marker pour signaler la position de parking de l'utilisateur
				 * @param lat
				 * @param lng
				 */
				_self.makeUserSpotMarker = function(lat, lng){
					$rootScope.map.addMarker({
						//'marker': $scope.spots[i].marker,
						'position': $scope.setPosition(lat, lng),
						'icon': {
							url : './img/markers/icn_md_marker_user.png',
							size:{
								width: 50,
								height: 58
							}
						},
						'title' : 'Vous êtes garé ici'
					}, function (marker) {
						//$log.debug('$rootScope.addMarker ', marker);
						$scope.userSpotMarker = marker;
					});
				};


				/**
				 * Callback du timer (chaque seconde)
				 */
				_self.countdownFunction = function () {
					_self.timeRemaining = Math.floor(($scope.spotEnCoursEndTime - (new Date())) / 1000);
					if (_self.timeRemaining <= 1) { // Si le temps est passé, on supprime tout et on revient sur l'écran de proposition
						_self.deleteSpotEnCours();
					}

					// Texte du timer
					_self.remainingTimeText = moment(_self.timeRemaining * 1000).format('mm:ss');

					$scope.progress.label = _self.remainingTimeText;
					$scope.progress.current = _self.timeRemaining;

					_self.timeRemaining--;
				};

				/**
				 * Fonction d'annulation de stationnement
				 * Il suffit de mettre le temps restant à 0 pour que le timer effectue les actions nécessaires une
				 * fois le timer nul (Voir code du timer pour les détails (<=0))
				 */
				$rootScope.annulerPropositionSpot = function () {
					var confirmPopup = {
						title: 'Proposer une place',
						message: 'Êtes-vous sûr(e) de vouloir annuler votre proposition de place de parking ?'
					};

					$cordovaDialogs.confirm(confirmPopup.message, confirmPopup.title, ['Oui', 'Non']).then(function (buttonIndex) {
						if (buttonIndex === 1) {
							$ionicLoading.show();
							SpotService.deleteSpot().then(function () {
								_self.deleteSpotEnCours();
								$ionicLoading.hide();
								$log.log('delete => OK');
							}, function (error) {
								$log.log('delete => KO');
								$ionicLoading.hide();
							});
						}
					});
				};

				/**
				 * Fonction de suppression du spot en cours
				 * Utilisé quand le countdown est terminé ou quand l'utilisateur annule son spot ( $scope.annuler() )
				 */
				_self.deleteSpotEnCours = function(){
					if($rootScope.networkAvailable) {
						$interval.cancel(_self.interval);
						_self.interval = null;
						SpotService.deleteSpot().then(function successCallback() {
							$localStorage.remove('_st');
							$localStorage.remove('_st_spot');
							if ($scope.userSpotMarker) {$scope.userSpotMarker.remove();}
							$rootScope.startGetSpots();
							$scope.spot = null;
							$rootScope.spotEnCours = null;
							$scope.hideSpotPropositionButton = false;
						}, function errorCallback(error) {
							$log.error('StreetRechercheController.deleteSpotEnCours : SpotService.deleteSpot() failed !!', error);
						});
						$rootScope.waitingPerson = false;
						$timeout(function(){
							$rootScope.map.refreshLayout();
						}, 500);
					} else{
						$rootScope.Utils.showNetworkErrorMsg();
					}
				};

				/**
				 * Retourne le temps restant à partir d'un timestamp sous le format HH:MM:SS
				 * @param time
				 * @returns {string}
				 */
				/*$scope.timeRemaining = function (time) {
					var t = moment(time * 1000).diff(moment()) / 1000;
					return ('0' + (Math.floor(t / 3600))).slice(-2) + ':'
							+ ('0' + (Math.floor((t % 3600) / 60))).slice(-2) + ':'
							+ ('0' + (Math.floor(((t % 3600) % 60)))).slice(-2);
				};*/

				/**
				 * Fonction permettant d'obtenir une adresse à partir d'une position GPS
				 */
				$scope.geocodeAdresse = function (coords) {
					if ($scope.geocodeTimeout) {$timeout.cancel($scope.geocodeTimeout);}

					if($rootScope.networkAvailable) {
						$scope.geocodeTimeout = $timeout(function () {
							$scope.geocodingAdresse = true;
							PropositionService.geocodeCoords(coords).then(function (place) {
								$scope.localisation = {
									numRue: PropositionService.numRue || '',
									route: PropositionService.route || '',
									cp: PropositionService.codePostal || '',
									ville: PropositionService.ville || '',
									departement: PropositionService.departement || '',
									region: PropositionService.region || '',
									pays: PropositionService.pays || '',
									adresseCourte: (PropositionService.numRue ? PropositionService.numRue + ' ' : '') + (PropositionService.route || '') + ', ' + PropositionService.ville,
									adresseLongue: PropositionService.formatted_address
								};
								$rootScope.localisation = $scope.localisation;
								$scope.geocodingAdresse = false;
							}, function (error) {
								$scope.localisation = {
									numRue: null,
									route: null,
									cp: null,
									ville: null,
									departement: null,
									region: null,
									pays: null,
									adresseCourte: 'Adresse inconnue',
									adresseLongue: 'Adresse inconnue'
								};
							});
						}, 100);
					}
				};

				/****************************************************
				 * RECHERCHE D'ADRESSE
				 ***************************************************/

				$ionicModal.fromTemplateUrl('street/recherche/modal-recherche-adresse.html', function ($ionicModal) {
					$scope.rechercheAdresseModal = $ionicModal;
				}, {
					id: 'rechercheAdresseModal',
					scope: $scope,
					animation: 'slide-in-up',
					focusFirstInput: true
				});

				$scope.$on('modal.hidden', function(event, modal) {
					$log.debug('modal '+modal.id+' hidden');
					if(modal.id === 'rechercheAdresseModal'){
						if(typeof $rootScope.map !== 'undefined') {$rootScope.map.setClickable(true);}
					} else if(modal.id === 'modalDemande'){
						if(typeof $rootScope.map !== 'undefined') {$rootScope.map.setClickable(true);}
					} else if(modal.id === 'modalAcceptation'){
						if(typeof $rootScope.map !== 'undefined') {$rootScope.map.setClickable(true);}
					} else if(modal.id === 'modalTuto'){
						if(typeof $rootScope.map !== 'undefined') {$rootScope.map.setClickable(true);}
					}
				});

				$scope.openModalRechercheAdresse = function(){


					if(typeof $scope.map !== 'undefined') {$rootScope.map.setClickable(false);}

					$scope.adressesRecentes = $localStorage.getObject('adressesRecentes', []);

					$scope.rechercheAdresseModal.show();
				};
				$scope.closeModalRechercheAdresse = function(){
					$log.debug('closeModalRechercheAdresse()');
					$scope.rechercheAdresseModal.hide();
				};

				$scope.selectMyLocation = function(){
					$scope.closeModalRechercheAdresse();
					$scope.goToMyLocation();
				};

				/**
				 Au clic sur une proposition d'adresse
				 * @param location
				 */
				$scope.selectLocation = function(location){
					$scope.closeModalRechercheAdresse();

					var adressesRecentes = $localStorage.getObject('adressesRecentes', []);
					var exists = adressesRecentes.find(function (d) {
						return d.id === location.id;
					});
					if(!exists){
						if(adressesRecentes.length >= 3) {adressesRecentes.pop();}
						adressesRecentes.unshift(location);
					}
					$localStorage.setObject('adressesRecentes', adressesRecentes);

					$scope.geocoder.geocode({'placeId': location.place_id}, function(results, status) {
						if (status === google.maps.GeocoderStatus.OK) {
							if (results[0]) {
								location = results[0];
								if(typeof $rootScope.map !== 'undefined') {
									_self.animateCamera(location.geometry.location.lat(), location.geometry.location.lng());
								}
							}
						}
					});
				};


				//MapsService.loadMapsAPI().then(function(){
				if(google) {
					$scope.autocompleteService = new google.maps.places.AutocompleteService();
					$scope.geocoder = new google.maps.Geocoder();
					//MapsService.mapsApiLoaded = true;
					$log.debug('Maps API loaded');
				}
				//});

				/**
				 * Recherche des suggestion à partir de l'adresse saisie
				 * @param query
				 */
				$scope.searchAdresse = function (query) {
					if ($scope.searchEventTimeout) {$timeout.cancel($scope.searchEventTimeout);}
					$scope.searchEventTimeout = $timeout(function() {

						if(!query || !query.length) {
							$scope.locations = [];
							return;
						}
						if(query.length < 3) {return;}
						$scope.searchingAdresse = true;
							$scope.autocompleteService.getQueryPredictions({
								input: query,
								componentRestrictions: {
									country: 'FR'
								}
							}, function (results, status) {
								if (status === google.maps.places.PlacesServiceStatus.OK) {
									$scope.$apply(function () {
										$scope.locations = results;
										$scope.searchingAdresse = false;
									});
								} else {
									$scope.$apply(function () {
										$scope.locations = [];
										$scope.searchingAdresse = false;
									});
								}
							});
					}, 350);
				};
			}
		]);
