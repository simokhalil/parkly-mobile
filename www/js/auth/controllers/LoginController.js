/**
 * Created by kHaLiL on 09/03/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.controllers = parkly.modules.controllers || angular.module('parkly.controllers', []);

// Création du controleur pour la vue Login
parkly.modules.controllers
		.controller('LoginController', ['$scope', '$state', '$stateParams', '$ionicPopover', '$log', '$ionicHistory', '$rootScope',
				'$ionicLoading', 'AuthenticationService', '$cordovaDialogs',
			function ($scope, $state, $stateParams, $ionicPopover, $log, $ionicHistory, $rootScope,
					  $ionicLoading, AuthenticationService, $cordovaDialogs) {

			$rootScope.title = 'Inscription / Connexion';

			$scope.context = $stateParams.context || 'login';

			$scope.loginData = {
				email: '',
				password: '',
				first_name: '',
				last_name: ''
			};

			$scope.loginError = false;
			$scope.loginErrorText = null;

			$scope.login = function () {
				$ionicLoading.show();
				var credentials = {
					email: $scope.loginData.email,
					password: $scope.loginData.password
				};

				if($rootScope.networkAvailable) {
					AuthenticationService.login(credentials).then(function (user) {
						$ionicHistory.nextViewOptions({
							disableBack: true
						});

						$ionicLoading.hide();

						AuthenticationService.goToHomeOrUserCar(user);
					}, function(err){
						$rootScope.loginError = true;
						if (err && err.error) {
							$log.warn('AuthenticationService.getUser : ', err);
							$rootScope.loginErrorText = err.error;
						} else {
							$rootScope.loginErrorText = 'Une erreur est survenue lors de l\'authentification';
						}
						$log.warn('AuthenticationService.getUser : ', $rootScope.loginErrorText);

						$ionicLoading.hide();
						$rootScope.Utils.showToast($rootScope.loginErrorText, 'Erreur d\'authentification', 'OK');
					}).catch(function (error) {
						$cordovaDialogs.alert('Un problème est survenu lors de l\'authentification. Veuillez réessayer plus tard.', 'Erreur d\'authentification', 'OK');
						$ionicLoading.hide();
					});
				} else{
					$rootScope.Utils.showNetworkErrorMsg();
				}
			}; // $scope.login

			$scope.register = function () {
				$log.debug('handling register...');
				$ionicLoading.show();
				var userData = {
					email: $scope.loginData.email,
					password: $scope.loginData.password,
					first_name: $scope.loginData.first_name,
					last_name: $scope.loginData.last_name
				};

				if($rootScope.networkAvailable) {
					AuthenticationService.register(userData).then(function (response) {
						$ionicLoading.hide();
						$scope.loginData.password = '';
						$scope.loginData.first_name = '';
						$scope.loginData.last_name = '';
						$cordovaDialogs.alert('Votre compte a été créé avec succès. Vous pouvez vous connecter.', 'Inscription réussie', 'OK')
								.then(function () {
									$scope.showLogin();
								});
					}).catch(function (error) {
						$ionicLoading.hide();
						$cordovaDialogs.alert(error, 'Erreur d\'inscription', 'OK');
					});
				} else{
					$rootScope.Utils.showNetworkErrorMsg();
				}
			};

			$scope.handle = function (context) {
				$log.debug('handle(' + context + ')');
				switch (context) {
					case 'login':
						$scope.login();
						break;
					case 'register':
						$scope.register();
						break;
				}
			};

			$scope.showRegister = function () {
				$log.debug('showRegister()');
				$scope.context = 'register';
			};
			$scope.showLogin = function () {
				$log.debug('showLogin()');
				$scope.context = 'login';
			};
			$scope.goToResetPassword = function () {
				$state.go('password');
			};

			$scope.buttonDisabled = function () {
				var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				switch ($scope.context) {
					case 'login':
						return !re.test($scope.loginData.email) || $scope.loginData.password.length < 3;
					case 'register':
						return !re.test($scope.loginData.email)
								|| $scope.loginData.password.length < 3
								|| $scope.loginData.first_name.length < 3
								|| $scope.loginData.last_name.length < 3;
				}
			};

		}]);
