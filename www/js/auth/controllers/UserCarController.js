/**
 * Created by kHaLiL on 09/03/2016.
 */

var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.controllers = parkly.modules.controllers || angular.module('parkly.controllers', []);
parkly.modules.controllers
		.controller('UserCarController', ['$scope', '$state', '$http', '$rootScope', 'APIBaseURL', '$localStorage', '$log', '$cordovaDialogs',
			'UserService',
			function ($scope, $state, $http, $rootScope, APIBaseURL, $localStorage, $log, $cordovaDialogs, UserService) {
			
			$scope.marqueActive = false;
			$scope.modeleActive = false;
			$scope.couleurActive = false;
			$scope.validerActive = false;

			$scope.marquesLoading = false;
			$scope.modelesLoading = false;

			$scope.voiture = {
				marque: $rootScope.currentUser.voitures[0] ? $rootScope.currentUser.voitures[0].marque : null,
				modele : $rootScope.currentUser.voitures[0] ? $rootScope.currentUser.voitures[0].modele : null,
				couleur: $rootScope.currentUser.voitures[0] ? $rootScope.currentUser.voitures[0].couleur : null
			};

			// Charger les marques depuis le serveur
			if($rootScope.networkAvailable) {
				$scope.marquesLoading = true;
				$http.get(APIBaseURL + '/cars/marques').then(function (response) {
					$scope.marques = response.data.marques;
					$scope.marquesLoading = false;
					$scope.marqueActive = true;

					// cache des marques
					$localStorage.setObject('marques', response.data.marques);

					if ($scope.voiture && $scope.voiture.marque && $scope.voiture.modele && Object.keys($scope.marques).length) {
						$http.get(APIBaseURL + '/cars/marques/' + $scope.voiture.marque + '/modeles').then(function (response) {
							$scope.modeles = response.data.modeles;
							$scope.modelesLoading = false;
							$scope.modeleActive = true;
							$scope.couleurActive = true;

							// cache des modèles de la marque du user
							$localStorage.setObject('modeles', response.data.modeles);
						}, function(error){
							$scope.modelesLoaded = true;
						});
					} else{
						$scope.modelesLoaded = true;
					}

				}, function (error) {
					$scope.marquesLoading = false;
					if ($localStorage.getObject('marques', null)) {
						$scope.marques = $localStorage.getObject('marques', null);
						$scope.marqueActive = true;

						$scope.modeles = $localStorage.getObject('modeles', null);
						$scope.modelesLoading = false;
						$scope.modeleActive = true;
					} else {
						$cordovaDialogs.alert('Impossible de récupérer la liste des marques. Vérifiez que vous êtes connecté(e).', 'Erreur');
					}
				});
				
				$rootScope.Utils.updateColors().then(function(formattedColors){
					var cachedCouleurs = $localStorage.getObject('couleurs', null);
					if(cachedCouleurs){
						$scope.couleurs = cachedCouleurs;
					}
				});
			} else{
				$rootScope.Utils.showNetworkErrorMsg();
			}


			$scope.marqueChanged = function(){
				if($rootScope.networkAvailable) {
					$scope.modelesLoading = true;
					$http.get(APIBaseURL + '/cars/marques/' + $scope.voiture.marque + '/modeles').then(function (response) {
						$scope.modeles = response.data.modeles;
						$scope.modelesLoading = false;
						$scope.modeleActive = true;
						// cache des marques
						$localStorage.setObject('modeles', response.data.modeles);
					});
				} else{
					$rootScope.Utils.showNetworkErrorMsg();
				}
			};

			$scope.modeleChanged = function(){
				$log.info('Profile : modeleChanged()');
				$scope.couleurActive = true;
			};

			$scope.couleurChanged = function(){
				$log.info('Profile : couleurChanged()');
				$scope.validerActive = true;
			};

			$scope.isValiderActive = function(){
				return $scope.voiture.marque && $scope.voiture.modele && $scope.voiture.couleur;
			};

			$scope.valider = function(){
				if($rootScope.networkAvailable) {
					if ($scope.voiture.marque && $scope.voiture.modele && $scope.voiture.couleur) {
						$rootScope.currentUser.voitures[0] = {
							marque: $scope.voiture.marque,
							modele: $scope.voiture.modele,
							couleur: $scope.voiture.couleur
						};

						if ($rootScope.networkAvailable) {
							UserService.editUser($rootScope.currentUser).then(function (response) {
								if($state.params.fromProfile) {$state.goBack();}
								else {$state.go('app.je-cherche');}
							}, function (error) {
								window.plugins.toast.showLongBottom('Une erreur est survenue. Veuillez réessayer....', function (a) {

								}, function (error) {
									$cordovaDialogs.alert('Une erreur est survenue. Veuillez réessayer.', 'Erreur');
								});
							});
						} else {
							$rootScope.Utils.showNetworkErrorMsg();
						}
					}
				} else{
					$rootScope.Utils.showNetworkErrorMsg();
				}
			};
		}]);
