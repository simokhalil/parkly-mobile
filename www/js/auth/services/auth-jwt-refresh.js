/**
 * Created by kHaLiL on 13/03/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.services = parkly.modules.services || angular.module('parkly.services', []);

// Création du controleur pour la vue Login
parkly.modules.services
		.factory('jwtRefreshService', ['$rootScope', '$http', 'authService', 'APIBaseURL', '$localStorage', '$state', '$ionicHistory', '$ionicLoading', '$log',
			function($rootScope, $http, authService, APIBaseURL, $localStorage, $state, $ionicHistory, $ionicLoading, $log) {

				return {
					refresh: function () {
						$log.debug('Token : Refresh...');
						var token = $localStorage.get('_satellizer_token', null);
						if (token && token !== 'undefined') {
							$http({
								method: 'GET',
								url: APIBaseURL + '/authenticate/token',
								headers: {
									'Authorization': 'Bearer ' + token
								}
							}).then(function (payload) {
								$localStorage.set('satellizer_token', payload.data.token);
								$localStorage.set('_satellizer_token', payload.data.token);
								$log.debug('Token : Refresh... OK');
								authService.loginConfirmed();
							}, function(error){
								$log.debug('Token : Refresh... KO');
								authService.loginCancelled(error, 'Token blacklisted');
								$rootScope.logout();
							});
						} else {
							$ionicLoading.hide();
							$ionicHistory.nextViewOptions({
								disableBack: true
							});
							$state.go('welcome');
						}
					},

					available: function () {
						return $http({
							method: 'GET',
							url: APIBaseURL + '/authenticate/available'
							//params: {token: $localStorage.get('_satellizer_token', null)}
						});
					}
				};
			}
		]);
