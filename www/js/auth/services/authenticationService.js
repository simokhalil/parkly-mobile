/**
 * Created by kHaLiL on 07/04/2016.
 */

var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.services = parkly.modules.services || angular.module('parkly.services', []);

// Création du service
parkly.modules.services
		.factory('AuthenticationService', ['$http', '$auth', '$q', '$cordovaDevice', 'APIBaseURL', '$ionicPlatform', '$ionicHistory', '$location',
			'$rootScope', '$localStorage', 'NotificationService', '$state', '$ionicLoading', '$log',
			function ($http, $auth, $q, $cordovaDevice, APIBaseURL, $ionicPlatform, $ionicHistory, $location,
					  $rootScope, $localStorage, NotificationService, $state, $ionicLoading, $log) {
				return {
					requireLogin: function () {
						return $q(function (resolve, reject) {
							if ($auth.isAuthenticated()) {
								resolve(true);
							} else {
								// unauthenticated - initiate login
								reject(); // thanks to this the state will not be changed
								$log.debug('Require Login : Return url: ' + $location.path());
								
								$ionicHistory.nextViewOptions({
									disableBack: true,
									historyRoot: true
								});
								
								// go to the login screen and provide return url (important for views others than the dashboard)
								$state.go('welcome', {returnUrl: $location.path()});
							}
						});
					},
					
					login: function (user) {
						var deferred = $q.defer();
						$log.debug('AuthenticationService.login()');
						
						$http({
							method: 'post',
							url: APIBaseURL + '/authenticate',
							timeout: 10000,
							data: user,
							ignoreAuthModule: true
						}).success(function (response) {
							// set a copy of the token to use for refresh requests
							$localStorage.set('_satellizer_token', response.token);
							$localStorage.set('satellizer_token', response.token);
							$localStorage.setObject('user', response.user);
							$rootScope.currentUser = response.user;
							
							deferred.resolve(response.user);
						}).error(function (err) {
							deferred.reject(err || 'Serveur injoignable !');
						});
						
						return deferred.promise;
					},
					
					fbLogin: function (user) {
						var deferred = $q.defer();
						$log.debug('AuthenticationService.FbLogin()');
						
						$http({
							method: 'post',
							url: APIBaseURL + '/authenticate/facebook',
							timeout: 10000,
							data: user,
							ignoreAuthModule: true
						}).success(function (response) {
							// set a copy of the token to use for refresh requests
							$localStorage.set('_satellizer_token', response.token);
							$localStorage.set('satellizer_token', response.token);
							$localStorage.setObject('user', response.user);
							$rootScope.currentUser = response.user;
							
							deferred.resolve(response.user);
						}).error(function (err) {
							deferred.reject(err || 'Serveur injoignable !');
						});
						
						return deferred.promise;
					},
					
					googleLogin: function (user) {
						var deferred = $q.defer();
						$log.debug('AuthenticationService.googleLogin()');
						
						$http({
							method: 'post',
							url: APIBaseURL + '/authenticate/google',
							timeout: 10000,
							data: user,
							ignoreAuthModule: true
						}).success(function (response) {
							// set a copy of the token to use for refresh requests
							$localStorage.set('_satellizer_token', response.token);
							$localStorage.set('satellizer_token', response.token);
							$localStorage.setObject('user', response.user);
							$rootScope.currentUser = response.user;
							
							deferred.resolve(response.user);
						}).error(function (err) {
							deferred.reject(err || 'Serveur injoignable !');
						});
						
						return deferred.promise;
					},
					
					register: function (user) {
						var deferred = $q.defer();
						$log.debug('AuthenticationService.register()');
						
						$http({
							method: 'post',
							url: APIBaseURL + '/register',
							timeout: 10000,
							data: {user: user},
							ignoreAuthModule: true
						}).success(function (response) {
							deferred.resolve(response.success);
						}).error(function (err) {
							deferred.reject(err.message);
						});
						
						return deferred.promise;
					},
					
					resetPassword: function (user) {
						var deferred = $q.defer();
						$log.debug('AuthenticationService.resetPassword()');
						
						$http({
							method: 'post',
							url: APIBaseURL + '/password/reset',
							timeout: 10000,
							data: user,
							ignoreAuthModule: true
						}).success(function (response) {
							deferred.resolve(response.success);
						}).error(function (err) {
							deferred.reject(err.message);
						});
						
						return deferred.promise;
					},
					
					logout: function () {
						var deferred = $q.defer();
						
						$ionicPlatform.ready(function () {
							if ($cordovaDevice && typeof device !== 'undefined') {
								$http.delete(APIBaseURL + '/devices/' + $cordovaDevice.getUUID())
										.success(function (data, status) {
											$log.debug('AuthenticationService.logout : Token removed from server');
											deferred.resolve(data);
										})
										.error(function (data, status) {
											$log.warn('AuthenticationService.logout : Error removing device token from server.' + data + ' ' + status);
											deferred.reject('Error removing device token.');
										});
							} else {
								deferred.resolve();
							}
						});
						
						
						return deferred.promise;
					},
					
					getLocalUser: function () {
						return $localStorage.getObject('user', null);
					},
					setUser: function (user) {
						var deferred = $q.defer();
						$rootScope.currentUser = user;
						$localStorage.setObject(user);
						deferred.resolve(user);
						return deferred.promise;
					},
					
					getUser: function () {
						var deferred = $q.defer();
						
						$http.get(APIBaseURL + '/authenticate/user').then(function (response) {
							// Stringify the returned data
							var user = response.data.user;
							
							// Set the stringified user data into local storage
							$localStorage.setObject('user', user);
							
							// Getting current user data from local storage
							$rootScope.currentUser = response.data.user;
							
							deferred.resolve(response.data.user);
						}, function (error) {
							var cachedUser = $localStorage.getObject('user', null);
							if (cachedUser) {
								$rootScope.currentUser = cachedUser;
								deferred.resolve(cachedUser);
							} else {
								deferred.reject(error);
							}
						}).catch(function (error) {
							var cachedUser = $localStorage.getObject('user', null);
							if (cachedUser) {
								$rootScope.currentUser = cachedUser;
								deferred.resolve(cachedUser);
							} else {
								deferred.reject('Impossible de joindre le serveur. Vérifiez que vous êtes connecté, puis réessayez');
							}
						});
						
						return deferred.promise;
					},

					goToHomeOrUserCar : function(user){
						if(!user.voitures || user.voitures.length <= 0 || !user.voitures[0] || !user.voitures[0].marque || !user.voitures[0].modele){
							$state.go('user-car', {disableBackButton: true});
						} else {
							$state.go('app.je-cherche');
						}
					},
					
					/**************************************************
					 * FACEBOOK LOGIN
					 *************************************************/
					
					// This method is to get the user profile info from the facebook api
					getFacebookProfileInfo: function (authResponse) {
						var info = $q.defer();
						
						facebookConnectPlugin.api('/me?fields=email,name,first_name,last_name,gender&access_token=' + authResponse.accessToken, ['email', 'public_profile'],
								function (response) {
									$log.debug('AuthenticationService.getFacebookProfileInfo : ', response);
									info.resolve(response);
								},
								function (response) {
									$log.warn('AuthenticationService.getFacebookProfileInfo : ', response);
									info.reject(response);
								}
						);
						return info.promise;
					}
				};
			}
		]);
