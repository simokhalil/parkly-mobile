var parkly = parkly || {};
parkly.modules = parkly.modules || {};

parkly.modules.app = angular.module('parkly', ['ionic', 'parkly.controllers', 'parkly.filters', 'parkly.directives', 'parkly.services', 'templates',
	'ngCordova', 'satellizer', 'http-auth-interceptor', 'angular-svg-round-progressbar','nlFramework', 'rzModule', 'ionic.ion.imageCacheFactory']);

parkly.modules.app
		.run(['$ionicPlatform', '$rootScope', '$state', '$ionicConfig', '$cordovaStatusbar', '$ionicPopup', 'jwtRefreshService', '$ionicHistory',
			'$localStorage', '$timeout', '$log', 'Utils', 'BASEURL', '$cordovaNetwork', '$auth',
			function ($ionicPlatform, $rootScope, $state, $ionicConfig, $cordovaStatusbar, $ionicPopup, jwtRefreshService, $ionicHistory,
					   $localStorage, $timeout, $log, Utils, BASEURL, $cordovaNetwork, $auth) {

				$ionicPlatform.ready(function () {

					// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
					// for form inputs)
					if (window.cordova) {
						if (window.cordova.plugins.Keyboard) {
							cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
							cordova.plugins.Keyboard.disableScroll(true);
						}

						$cordovaStatusbar.styleHex('#1C262E');

						$rootScope.$on('$cordovaNetwork:online', function(event, networkState){
							$log.debug('App is online PLUGIN');
							$rootScope.networkAvailable = true;
							if($state.current.name === 'app.je-cherche') {$rootScope.startGetSpots();}
						});

						// listen for Offline event
						$rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
							$log.debug('App is offline PLUGIN');
							$rootScope.networkAvailable = false;
							$rootScope.stopGetSpots();
							if($rootScope.map) {$rootScope.map.clear();}
						});

						document.addEventListener('pause', function() {
							$log.debug('onPause');
							if(typeof $rootScope.stopGetSpots !== 'undefined') {$rootScope.stopGetSpots();}
							if(typeof $rootScope.map !== 'undefined') {$rootScope.map.clear();}
						}, false);

						document.addEventListener('resume', function() {
							$log.debug('onResume');
							if($state.current.name === 'app.je-cherche') {$rootScope.startGetSpots();}
							/*var end = $localStorage.getInt('_st', null);
							if(end){
								var now = new Date();
								$localStorage.set('_st_ttl', Math.floor((end - now) / 1000));
							}*/
						}, false);

						$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
							var excludedViews = ['startup', 'login', 'welcome', 'password'];
							if (!$auth.isAuthenticated() && excludedViews.indexOf(toState.name) === -1) {
								$log.debug('App.js : Event => $onStateChangeStart()');
								event.preventDefault();
								$log.warn('App.js : event:auth-loginRequired');

								//$rootScope.$broadcast('event:auth-loginRequired');

								if(!$localStorage.get('_satellizer_token')){
									$ionicHistory.nextViewOptions({
										disableBack: true,
										historyRoot: true
									});
									$state.go('welcome');
								} else{
									$rootScope.$broadcast('event:auth-loginRequired');
								}
							}
						});
					}

					// Enable views cache for 4 views
					$ionicConfig.views.maxCache(4);

					if (!$localStorage.get('_u_cur_usages')) {
						$log.debug('App.js : _u_cur_usages = ' + $localStorage.get('_u_cur_usages'));
						$localStorage.set('_u_cur_usages', 0);
					}
					$rootScope.userCurrentUsages = parseInt($localStorage.get('_u_cur_usages'));

					if($cordovaNetwork.isOnline()){
						$rootScope.networkAvailable = true;
					} else if($cordovaNetwork.isOnline()){
						$rootScope.networkAvailable = false;
					}

					$rootScope.geolocationAvailable = true;

					/*if (window.StatusBar) {
					 $log.debug('App.js : window.StatusBar => OK');
					 StatusBar.styleDefault();
					 if (cordova.platformId == 'android') {
					 $log.debug('App.js : window.StatusBar pour Android => OK');
					 StatusBar.backgroundColorByHexString("#ed8323");
					 }
					 }*/


					// Check for network connection
					/*if (window.Connection) {
						if (navigator.connection.type == Connection.NONE) {
							$ionicPopup.confirm({
								title: 'No Internet Connection',
								content: 'Sorry, no Internet connectivity detected. Please reconnect and try again.'
							})
							.then(function (result) {
								if (!result) {
									ionic.Platform.exitApp();
								}
							});
						}

						$rootScope.networkAvailable = navigator.connection.type == "none" ? false : true;
					}*/

					$rootScope.Utils = Utils;
					$rootScope.baseURL = BASEURL;

					// Register du bouton Retour
					$rootScope.backButtonTaps = 0;
					$ionicPlatform.registerBackButtonAction(function () {
						if (!$rootScope.drawer || !$rootScope.drawer.openned ) {
							var excludedViews = ['startup', 'welcome', 'app.je-cherche'];
							var disableBackButton = $state.params.disableBackButton || false;

							if(excludedViews.indexOf($state.current.name) === -1 && !disableBackButton){
								if($state.current.name === 'app.je-cherche'){
									// Implémenter les actions du boutons Retour sur la map
									// A revoir car mal placé ici
								}

								if (typeof $state.current.params.allowBack === 'undefined' || (!$state.current.params.allowBack && !$state.params.allowBack)) {
									$ionicHistory.nextViewOptions({
										disableBack: true,
										historyRoot: true
									});
									$state.go('app.je-cherche');
								} else {
									$ionicHistory.goBack();
								}
							} else {
								if ($rootScope.backButtonTaps === 0) {
									$rootScope.backButtonTaps++;

									// show the notification
									window.plugins.toast.showLongBottom('Appuyez à nouveau sur Retour pour fermer l\'application', function (a) {
										$log.debug('Back button : Toast OK');
									}, function (error) {
										navigator.app.exitApp();
									});
									$timeout(function () {
										$rootScope.backButtonTaps = 0;
										$log.debug('Back button : Réinitialisation OK');
									}, 3000);
								} else {
									navigator.app.exitApp();
								}
							}
						} else {
							// thedrawer is openned - close
							$rootScope.drawer.hide();
						}
					}, 100);


					// Refresh authorization token when it is expired transparently to the user
					// and re-run the request that failed (happens automatically from http-auth-interceptor)
					$rootScope.$on('event:auth-loginRequired', function (event, data) {
						jwtRefreshService.refresh();
					});

				});
			}
		]);
