/**
 * Created by kHaLiL on 09/03/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.controllers = parkly.modules.controllers || angular.module('parkly.controllers', []);

// Création du controleur pour la vue Login
parkly.modules.controllers
		.controller('AboutController', ['$scope', '$rootScope', '$state', '$ionicPlatform', function ($scope, $rootScope, $state, $ionicPlatform) {

			$scope.$on('$ionicView.beforeEnter', function(e) {
				$rootScope.viewTitle = 'A propos';
				$rootScope.showBackButton = $state.params.allowBack || false;
			});


			$ionicPlatform.ready(function () {
				cordova.getAppVersion.getVersionNumber().then(function (version) {
					$scope.appVersion = version;
				});
			});

			$scope.browse = function(url, title){
				$state.go('app.browser', {url: url, title: title});
			};

		}]);
