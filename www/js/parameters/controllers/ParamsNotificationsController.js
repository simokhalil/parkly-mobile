/**
 * Created by kHaLiL on 09/03/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.controllers = parkly.modules.controllers || angular.module('parkly.controllers', []);

// Création du controleur pour la vue Login
parkly.modules.controllers
		.controller('ParamsNotificationsController', ['$scope', '$rootScope', '$state', function ($scope, $rootScope, $state) {

			$scope.$on('$ionicView.beforeEnter', function() {
				$rootScope.viewTitle = 'Notifications';
				$rootScope.showBackButton = $state.params.allowBack || false;
			});

			$scope.$on('$ionicView.enter', function(){
				$scope.notifsReservations = true;
				$scope.notifsActivite = true;
			});

		}]);
