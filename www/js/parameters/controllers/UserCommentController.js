/**
 * Created by kHaLiL on 09/03/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.controllers = parkly.modules.controllers || angular.module('parkly.controllers', []);

// Création du controleur pour la vue Login
parkly.modules.controllers
		.controller('UserCommentController', ['$scope', '$rootScope', '$state', 'APIBaseURL', '$http',
			function ($scope, $rootScope, $state, APIBaseURL, $http) {

				$scope.$on('$ionicView.beforeEnter', function() {
					$rootScope.viewTitle = 'Commentaire';
					$rootScope.showBackButton = $state.params.allowBack || false;
				});

				$scope.sendStatus = {};
				$scope.data = {};
				$scope.data.userComment = '';

				$scope.sendMessage = function(){
					if($scope.data.userComment.length >= 5){
						if($rootScope.networkAvailable) {
							$http({
								method: 'POST',
								url: APIBaseURL + '/userMessage',
								data: {
									message: $scope.data.userComment
								}
							}).then(function (data) {
								$scope.sendStatus.type = 'success';
								$scope.sendStatus.message = 'Votre message a bien été envoyé. Merci!';
							}, function(error){
								$scope.sendStatus.type = 'error';
								$scope.sendStatus.message = 'Votre message n\'a pas pu être envoyé. Merci de réessayer ultérieurement.';

								$rootScope.Utils.showToast('Votre message n\'a pas pu être envoyé. Merci de réessayer ultérieurement.', 'Erreur');
							});
						} else{
							$rootScope.Utils.showNetworkErrorMsg();
						}
					}
				};

			}
		]);
