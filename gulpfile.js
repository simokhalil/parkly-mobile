var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var replace = require('gulp-replace-task');
var args    = require('yargs').argv;
var sequence = require('run-sequence');
var fs      = require('fs');
var templateCache = require('gulp-angular-templatecache');
var ngAnnotate = require('gulp-ng-annotate');
var useref = require('gulp-useref');
var del = require('del');
var jshint = require('gulp-jshint');
var jshint_stylish = require('jshint-stylish');

var paths = {
	sass: ['./scss/**/*.scss'],
	fonts: ['./www/css/fonts/*'],
	templateCache: ['./www/templates/**/*.html'],
	ng_annotate: ['./www/js/**/*.js'],
	useref: ['./www/*.html']
};

//gulp.task('default', ['clean:dist', 'sass', 'fonts', 'templatecache', 'ng_annotate', 'useref']);
/**
 * Permet de lancer les tâches de manière synchrone (Replace la ligne au dessus)
 */
gulp.task('default', function (done) {
	sequence('clean:dist', 'sass', 'fonts', 'templatecache', 'ng_annotate', 'useref', 'replace', done);
});

gulp.task('sass', function (done) {
	gulp.src('./scss/ionic.app.scss')
			.pipe(sass())
			.on('error', sass.logError)
			.pipe(gulp.dest('./www/css/'))
			.pipe(minifyCss({
				keepSpecialComments: 0
			}))
			.pipe(rename({extname: '.min.css'}))
			.pipe(gulp.dest('./www/css/'))
			.on('end', done);
});

gulp.task('watch', function () {
	gulp.watch(paths.sass, ['sass']);
	gulp.watch(paths.fonts, ['fonts']);
	gulp.watch(paths.templateCache, ['templatecache']);
	gulp.watch(paths.ng_annotate, ['ng_annotate']);
	gulp.watch(paths.useref, ['useref']);
});

gulp.task('install', ['git-check'], function () {
	return bower.commands.install()
			.on('log', function (data) {
				gutil.log('bower', gutil.colors.cyan(data.id), data.message);
			});
});

gulp.task('git-check', function (done) {
	if (!sh.which('git')) {
		console.log(
				'  ' + gutil.colors.red('Git is not installed.'),
				'\n  Git, the version control system, is required to download Ionic.',
				'\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
				'\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
		);
		process.exit(1);
	}
	done();
});

gulp.task('clean:dist', function(done){
	del('./www/dist/dist_js/*');
	del('./www/dist/dist_css/*');
	del('./www/dist/*.html');
	done();
});

gulp.task('templatecache', function (done) {
	gulp.src('./www/templates/**/*.html')
			.pipe(templateCache({standalone: true}))
			.pipe(gulp.dest('./www/js'))
			.on('end', done);
});

gulp.task('ng_annotate', function (done) {
	gulp.src('./www/js/**/*.js')
			.pipe(ngAnnotate({single_quotes: true}))
			.pipe(gulp.dest('./www/dist/dist_js/app'))
			.on('end', done);
});

gulp.task('useref', function (done) {
	gulp.src('./www/*.html')
			.pipe(useref())
			.pipe(gulp.dest('./www/dist'))
			.on('end', done);
});

/**
 * Permet de copier les fonts vers dist_css
 */
gulp.task('fonts', function(done){
	gulp.src('./www/css/fonts/*')
			.pipe(gulp.dest('./www/dist/dist_css/fonts'))
			.on('end', done);
});

/**
 * Permet de remplacer des variables dans le code (contants.js notamment) par celles décrites dans
 * les fichiers de configuration selon l'env souhaité. L'env est déterminé dans les arguments
 */
gulp.task('replace', function (done) {
	// Get the environment from the command line
	var env = args.env || 'homol';

	// Read the settings from the right file
	var filename = env + '.json';
	var settings = JSON.parse(fs.readFileSync('./config/' + filename, 'utf8'));

	// Replace each placeholder with the correct value for the variable.
	gulp.src('./www/dist/dist_js/app.js')
			.pipe(replace('@@apiUrl', settings.apiUrl))
			.pipe(replace('@@baseUrl', settings.baseUrl))
			.pipe(replace('defaultLocation', settings.defaultUrl))
			.pipe(gulp.dest('./www/dist/dist_js'))
			.on('end', done);
});

gulp.task('lint', function() {
	return gulp.src('./www/js/**/*.js')
			.pipe(jshint())
			.pipe(jshint.reporter('jshint-stylish'));
});
